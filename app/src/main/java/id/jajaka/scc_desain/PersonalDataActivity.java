package id.jajaka.scc_desain;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by Lando Rinov on 4/19/2016.
 */
public class PersonalDataActivity extends Activity{

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_personal_data);
    }
}
